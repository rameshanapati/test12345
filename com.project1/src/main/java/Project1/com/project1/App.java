package Project1.com.project1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Hello world!
 *
 */
public class App 
{
	@Test
    public  void main( ) throws InterruptedException
    {
    	ExtentReports ext=new ExtentReports("C:\\Users\\ramesh\\Desktop\\ReportsFolder\\repo.html");
        
        ExtentTest loggg=ext.startTest("Extent Reports Demo");
        
        System.out.println( "Hello Reports World!" );
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ramesh\\Desktop\\chromedriver_win32\\chromedriver.exe");
		
        WebDriver driver=new ChromeDriver();
        driver.get("https://www.gmail.com");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        loggg.log(LogStatus.INFO, "Opened browser");
        loggg.setDescription("Running ExtentReports 2.0");
        //loggg.addScreenCapture("C:\\Users\\ramesh\\Desktop\\Untitled.png");
        loggg.log(LogStatus.INFO, "Snapshot below: " + loggg.addScreenCapture("C:\\Users\\ramesh\\Desktop\\Untitled.png"));
        driver.findElement(By.id("Email")).sendKeys("atsunithase");
    Thread.sleep(1000);
    loggg.log(LogStatus.INFO, "uname entered");

    driver.findElement(By.id("next")).click();
    loggg.log(LogStatus.INFO, "clicked on next");

    driver.findElement(By.id("Passwd")).sendKeys("gggggg");
    
    loggg.log(LogStatus.INFO, "pwd entered");
    loggg.log(LogStatus.INFO, "Screencast below: " + loggg.addScreencast("C:\\Users\\ramesh\\Desktop\\Untitled.png"));

    Thread.sleep(1000);
    
    driver.findElement(By.id("signIn")).click();
    loggg.log(LogStatus.INFO, "clicked on signIn");
    ext.flush();
ext.endTest(loggg);
driver.get("file:///C:/Users/ramesh/Desktop/ReportsFolder/repo.html");

    }
}
